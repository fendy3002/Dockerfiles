docker network create -d bridge --subnet 192.168.0.0/16 dckr_net

docker run -d -e "MYSQL_ALLOW_EMPTY_PASSWORD=true" --name test_db mariadb:latest

docker network connect dckr_net test_db

sleep 20

docker run --rm --net dckr_net registry.gitlab.com/fendy3002/dockerfiles/qz/mariadbc:latest mysql -uroot -htest_db -e "create database test_db"

docker run --rm --net dckr_net -v `pwd`/test.conf:/conf/connection.conf -v `pwd`/testscript:/scripts registry.gitlab.com/fendy3002/dockerfiles/qz/mariadbc:latest deploy

docker run --rm --net dckr_net -v `pwd`/test.conf:/conf/connection.conf registry.gitlab.com/fendy3002/dockerfiles/qz/mariadbc:latest mariadbc "select * from test_db.user"

docker run --rm --net dckr_net -v `pwd`/test.conf:/conf/connection.conf -v `pwd`/testscript:/backup registry.gitlab.com/fendy3002/dockerfiles/qz/mariadbc:latest backup -n "db_backup.sql" 

rm -f ./testscript/db_backup.sql

docker stop test_db
docker rm test_db
docker network rm dckr_net