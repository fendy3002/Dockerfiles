#!/bin/bash
BASEDIR=$(dirname "$0")
GREEN='\033[0;32m'
NC='\033[0m' # No Color

if [ $# -eq 0 ]
  then
    printf "Please specify app name like: ./up.sh ${GREEN}app1${NC}\n"
else
    (cd $BASEDIR/../ && docker-compose -f common-service.yml -f ./apps/$1.yml up -d;)
fi

